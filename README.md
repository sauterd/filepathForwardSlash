`fpfs.exe` replaces all backslashes (`\`) of the clipboard content (strings only)
by forward slashes (`/`). This is useful when copying and pasting windows file
paths into an application that does not handle backslashes well (e.g. LaTex).