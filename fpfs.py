#! python3
# fpfs - Changes all backslashes in the clipboard to forward slashes.

import pyperclip

# Get string.
content = pyperclip.paste()

# Replace \ by /.
newContent = content.replace("\\", "/")
# Double backslash because one is used as escape character.

# Save to clipboard.
pyperclip.copy(newContent)
